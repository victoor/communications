<?php

namespace App;

use Carbon\Carbon;

class Communication
{
    private $type;
    private $from;
    private $to;
    private $direction;
    private $name;
    private $date;
    private $duration;

    public function __construct(string $log)
    {
        $this->type = $this->getSubString($log, 0, 1);
        $this->from = $this->getSubString($log , 1, 9);
        $this->to = $this->getSubString($log , 10, 9);
        $this->direction = $this->getSubString($log , 19, 1);
        $this->name = $this->getSubString($log , 20, 24);
        $this->duration = $this->getSubString($log , 58, 6);

        $this->setDate($log);
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return int
     */
    public function getDirection(): int
    {
        return (int) $this->direction;
    }

    /**
     * @return string
     */
    public function getDirectionString(): string
    {
        return $this->direction == 1 ? __('Received') : __('Made');
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return (int) $this->duration;
    }

    private function getSubString(string $log, $start, $length)
    {
        $stringPortion = substr($log, $start, $length);
        return $stringPortion ? trim($stringPortion) : null;
    }

    private function setDate(string $log)
    {
        $dateString = $this->getSubString($log, 44, 14);

        if ($dateString !== null) {
            $this->date = Carbon::createFromFormat('dmYHis', $dateString);
        }
    }
}