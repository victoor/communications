<?php

namespace App\Http\Services;

use App\Communication;
use Illuminate\Support\Collection;

class CommunicationParserService
{
    private $validTypes = ['C', 'S'];

    public function parse(array $log): Collection
    {
        foreach ($log as $communication) {
            $communication = new Communication($communication);

            if (in_array($communication->getType(), $this->validTypes)) {
                $communications[] = $communication;
            }
        }

        return (new Collection($communications))->groupBy(function(Communication $item, $key) {
            return $item->getDirection() == 1 ? $item->getFrom() : $item->getTo();
        });
    }
}