<?php

namespace App\Http\Services;

use App\Communication;
use App\Http\Daos\CommunicationsDao;
use Illuminate\Support\Collection;

class CommunicationService
{
    /**
     * @var CommunicationGetterService
     */
    private $communicationGetter;

    /**
     * @var CommunicationParserService
     */
    private $communicationParser;

    /**
     * @var CommunicationsDao
     */
    private $communicationsDao;

    public function __construct(
        CommunicationGetterService $communicationGetter,
        CommunicationParserService $communicationParser,
        CommunicationsDao $communicationsDao
    ) {
        $this->communicationGetter = $communicationGetter;
        $this->communicationParser = $communicationParser;
        $this->communicationsDao = $communicationsDao;
    }

    public function getCommunications(): Collection
    {
        foreach ($this->communicationsDao->all() as $phone => $file) {
            $logs = $this->communicationGetter->read($file);
            $communications[$phone] = $this->communicationParser->parse($logs);
        }

        return new Collection($communications);
    }
}