<?php

namespace App\Http\Services;

use App\Exceptions\UnableToOpenFileException;

class CommunicationGetterService
{
    /**
     * @throws UnableToOpenFileException
     */
    public function read(string $path): array
    {
        $file = $this->getFile($path);
        while (!feof($file)) {
            $content[] = trim(fgets($file));
        }
        fclose($file);

        return $content;
    }

    /**
     * @throws UnableToOpenFileException
     */
    private function getFile(string $path)
    {
        try {
            $file = fopen($path, "r");
        } catch (\Exception $exception) {
            throw new UnableToOpenFileException($exception->getMessage());
        }

        return $file;
    }
}