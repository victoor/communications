<?php

namespace App\Http\Controllers;

use App\Http\Services\CommunicationService;
use Illuminate\Http\Request;

class CommunicationsController extends Controller
{
    /**
     * @var CommunicationService
     */
    private $communicationService;

    public function __construct(CommunicationService $communicationService)
    {
        $this->communicationService = $communicationService;
    }

    public function __invoke(Request $request)
    {
        $communications = $this->communicationService->getCommunications();

        return view('communications.index', ['phones' => $communications]);
    }
}
