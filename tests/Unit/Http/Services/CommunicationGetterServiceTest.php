<?php

namespace Tests\Unit\Http\Services;

use App\Http\Services\CommunicationGetterService;
use Tests\TestCase;

class CommunicationGetterServiceTest extends TestCase
{
    /**
     * @var CommunicationGetterService
     */
    private $logReader;

    public function setUp()
    {
        $this->logReader = new CommunicationGetterService();
    }

    public function testReadWithAnExistingFile()
    {
        $anyFile = "https://gist.githubusercontent.com/miguelgf/e099e5e5bfde4f6428edb0ae94946c83/raw/fa27e59eb8f14ee131fca5c0c7332ff3b924e0b2/communications.611111111.log";
        $expected = [
            'C6112223336009998880Pepe                    01012016205203000142',
            'S7001112226112223331Movistar                02012016180130',
            'C9112223336112223331Mama                    03012016190000000142',
            'C9114445556112223331Rodrigo                 04012016200000000230',
            'C6112223336336667770Jose                    05012016200000000501',
            'S611222333     14200                        05012016220000',
            'U4815162342',
            'C6112223336336667770Javi                    06012016200000000110',
            'U4815162342',
            'C6112223336336667770Jose                    06012016200500000501',
            'S6112223336336667770Jose                    06012016220000',
            'C9112223336112223331Mama                    07012016190000013034',
        ];

        $result = $this->logReader->read($anyFile);

        $this->assertEquals($expected, $result);
    }

    /**
     * @expectedException \App\Exceptions\UnableToOpenFileException
     */
    public function testThatWeThrowAnExceptionWhenWeCanReachFile()
    {
        $nonExistingFile = "https://gist.githubusercontent.com/miguelgf/lalallalallalal.log";
        $this->logReader->read($nonExistingFile);
    }
}
