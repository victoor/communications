<?php

namespace Tests\Unit\Http\Services;

use App\Http\Daos\CommunicationsDao;
use App\Http\Services\CommunicationGetterService;
use App\Http\Services\CommunicationParserService;
use App\Http\Services\CommunicationService;
use Illuminate\Support\Collection;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class CommunicationServiceTest extends TestCase
{
    /**
     * @var CommunicationGetterService|MockInterface
     */
    private $communicationGetterService;

    /**
     * @var CommunicationParserService|MockInterface
     */
    private $communicationParserService;

    /**
     * @var CommunicationsDao|MockInterface
     */
    private $communicationsDao;

    /**
     * @var CommunicationService
     */
    private $communicationService;

    public function setUp()
    {
        $this->communicationGetterService = Mockery::mock(CommunicationGetterService::class);
        $this->communicationGetterService->shouldReceive('read')
            ->andReturn(['C6112223336009998880Pepe                    01012016205203000142']);

        $this->communicationParserService = Mockery::mock(CommunicationParserService::class);
        $this->communicationParserService->shouldReceive('parse')
            ->andReturn(Collection::make([]));

        $this->communicationsDao = Mockery::mock(CommunicationsDao::class);


        $this->communicationService = new CommunicationService(
            $this->communicationGetterService, $this->communicationParserService, $this->communicationsDao);
    }

    public function testGetCommunications()
    {
        $this->communicationsDao->shouldReceive('all')->andReturn(Collection::make([
            '666666666' => 'any.666666666.log',
            '999999999' => 'any.999999999.log',
        ]));

        $result = $this->communicationService->getCommunications();

        $this->assertEquals(2, $result->count());
    }
}
