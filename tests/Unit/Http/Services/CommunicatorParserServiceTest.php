<?php

namespace Tests\Unit\Http\Services;

use App\Http\Services\CommunicationParserService;
use Tests\TestCase;

class CommunicatorParserServiceTest extends TestCase
{
    /**
     * @var CommunicationParserService
     */
    private $communicationParserService;

    public function setUp()
    {
        $this->communicationParserService = new CommunicationParserService();
    }

    public function testThatParserReturnWhatWeExpect()
    {
        $return = $this->communicationParserService->parse($this->getLog());

        $this->assertEquals(6, $return->count());
    }

    private function getLog(): array
    {
        return [
            'C6112223336009998880Pepe                    01012016205203000142',
            'S7001112226112223331Movistar                02012016180130',
            'C9112223336112223331Mama                    03012016190000000142',
            'C9114445556112223331Rodrigo                 04012016200000000230',
            'C6112223336336667770Jose                    05012016200000000501',
            'S611222333     14200                        05012016220000',
            'U4815162342',
            'C6112223336336667770Javi                    06012016200000000110',
            'U4815162342',
            'C6112223336336667770Jose                    06012016200500000501',
            'S6112223336336667770Jose                    06012016220000',
            'C9112223336112223331Mama                    07012016190000013034',
        ];
    }
}
