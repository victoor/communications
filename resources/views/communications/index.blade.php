@extends('layouts.main')

@section('content')
    @foreach($phones as $phone => $comunications)
        <h3>{{ __('Phone') }}: {{ $phone }}</h3>

        @foreach($comunications as $contact => $comunicationLog)
            <table class="table">
                <thead>
                <tr>
                    <th>{{ __('Contact') }}: {{ $contact }} ({{ $comunicationLog->first()->getName() }})</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>{{ __('Direction') }}</th>
                                        <th>{{ __('Type') }}</th>
                                        <th>{{ __('Date') }}</th>
                                        <th>{{ __('Duration') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($comunicationLog as $comunication)
                                        <tr>
                                            <td>{{ $comunication->getDirectionString() }}</td>
                                            <td>{{ $comunication->getType() }}</td>
                                            <td>{{ $comunication->getDate() }}</td>
                                            <td>{{ $comunication->getDuration() }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        @endforeach
    @endforeach
@endsection